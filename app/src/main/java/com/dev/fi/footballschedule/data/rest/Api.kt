package com.dev.fi.footballschedule.data.rest

import com.dev.fi.footballschedule.data.model.AllLeague
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.data.model.TeamDetail
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

interface Api {
    @GET("all_leagues.php")
    fun allLeagues(): Flowable<Response<AllLeague.Leagues>>

    @GET("eventspastleague.php")
    fun eventsPastLeague(@QueryMap headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>>

    @GET("eventsnextleague.php")
    fun eventsNextLeague(@QueryMap headers: LinkedHashMap<String, String>): Flowable<Response<MatchEvent.Events>>

    @GET("lookupteam.php")
    fun teamDetail(@QueryMap headers: LinkedHashMap<String, String>): Flowable<Response<TeamDetail.Teams>>
}