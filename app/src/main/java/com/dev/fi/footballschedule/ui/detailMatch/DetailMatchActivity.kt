package com.dev.fi.footballschedule.ui.detailMatch

import android.view.Menu
import android.view.MenuItem
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.base.BaseActivity
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.databinding.ActivityDetailMatchBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailMatchActivity : BaseActivity<ActivityDetailMatchBinding>() {
    private val viewModel by viewModel<DetailMatchActivityModelView>()
    private var menuItem: Menu? = null

    lateinit var event: MatchEvent.Event

    override fun getToolbarResource(): Int = R.id.main_toolbar

    override fun getLayoutResource(): Int = R.layout.activity_detail_match

    override fun getToolbarTitle(): String = getString(R.string.title_detailMatch)

    override fun mainCode() {
        dataBinding.setLifecycleOwner(this)

        event = intent.extras.getParcelable("event")

        viewModel.loadLogoHome(event.idHomeTeam ?: "")
        viewModel.loadLogoAway(event.idAwayTeam ?: "")

        event.idEvent?.let { viewModel.favoriteState(this, it) }

        dataBinding.model = event
        dataBinding.viewModel = viewModel

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        if (menu != null) {
            viewModel.setFavorite(menu, this)
        }
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (viewModel.isFavorite) event.idEvent?.let {
                    viewModel.removeFromFavorite(this, it)
                } else viewModel.addToFavorite(this, event)

                viewModel.isFavorite = !viewModel.isFavorite
                menuItem?.let { viewModel.setFavorite(it, this) }

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


}
