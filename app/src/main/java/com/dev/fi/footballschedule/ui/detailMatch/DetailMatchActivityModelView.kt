package com.dev.fi.footballschedule.ui.detailMatch

import android.app.Activity
import android.database.sqlite.SQLiteConstraintException
import android.view.Menu
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.base.BaseViewModel
import com.dev.fi.footballschedule.data.database.FavoriteMatch
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.data.rest.Repository
import com.dev.fi.footballschedule.utils.database
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class DetailMatchActivityModelView(private val repository: Repository) : BaseViewModel() {
    var logoHome = MutableLiveData<String>()
    var logoAway = MutableLiveData<String>()
    var isFavorite: Boolean = false

    fun loadLogoHome(id: String) {
        composite {
            repository
                    .getTeamDetail(
                            linkedMapOf(
                                    "id" to id
                            ))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.code() == 200) {
                            val homeBadge = it.body()?.teams?.get(0)?.strTeamBadge.toString()
                            logoHome.value = homeBadge
                        }
                    }, {
                        //Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                    })
        }

    }

    fun loadLogoAway(id: String) {
        composite {
            repository
                    .getTeamDetail(
                            linkedMapOf(
                                    "id" to id
                            ))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.code() == 200) {
                            val awayBadge = it.body()?.teams?.get(0)?.strTeamBadge.toString()
                            logoAway.value = awayBadge
                        }
                    }, {
                        //Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                    })
        }

    }

    fun addToFavorite(activity: Activity, matchEvent: MatchEvent.Event) {
        try {
            activity.database.use {
                insert(FavoriteMatch.TABLE_FAVORITE,
                        FavoriteMatch.IDEVENT to matchEvent.idEvent,
                        FavoriteMatch.IDSOCCERXML to matchEvent.idSoccerXML,
                        FavoriteMatch.STREVENT to matchEvent.strEvent,
                        FavoriteMatch.STRFILENAME to matchEvent.strFilename,
                        FavoriteMatch.STRSPORT to matchEvent.strSport,
                        FavoriteMatch.IDLEAGUE to matchEvent.idLeague,
                        FavoriteMatch.STRLEAGUE to matchEvent.strLeague,
                        FavoriteMatch.STRSEASON to matchEvent.strSeason,
                        FavoriteMatch.STRDESCRIPTIONEN to matchEvent.strDescriptionEN,
                        FavoriteMatch.STRHOMETEAM to matchEvent.strHomeTeam,
                        FavoriteMatch.STRAWAYTEAM to matchEvent.strAwayTeam,
                        FavoriteMatch.INTHOMESCORE to matchEvent.intHomeScore,
                        FavoriteMatch.INTROUND to matchEvent.intRound,
                        FavoriteMatch.INTAWAYSCORE to matchEvent.intAwayScore,
                        FavoriteMatch.INTSPECTATORS to matchEvent.intSpectators,
                        FavoriteMatch.STRHOMEGOALDETAILS to matchEvent.strHomeGoalDetails,
                        FavoriteMatch.STRHOMEREDCARDS to matchEvent.strHomeRedCards,
                        FavoriteMatch.STRHOMEYELLOWCARDS to matchEvent.strHomeYellowCards,
                        FavoriteMatch.STRHOMELINEUPGOALKEEPER to matchEvent.strHomeLineupGoalkeeper,
                        FavoriteMatch.STRHOMELINEUPDEFENSE to matchEvent.strHomeLineupDefense,
                        FavoriteMatch.STRHOMELINEUPMIDFIELD to matchEvent.strHomeLineupMidfield,
                        FavoriteMatch.STRHOMELINEUPFORWARD to matchEvent.strHomeLineupForward,
                        FavoriteMatch.STRHOMELINEUPSUBSTITUTES to matchEvent.strHomeLineupSubstitutes,
                        FavoriteMatch.STRHOMEFORMATION to matchEvent.strHomeFormation,
                        FavoriteMatch.STRAWAYREDCARDS to matchEvent.strAwayRedCards,
                        FavoriteMatch.STRAWAYYELLOWCARDS to matchEvent.strAwayYellowCards,
                        FavoriteMatch.STRAWAYGOALDETAILS to matchEvent.strAwayGoalDetails,
                        FavoriteMatch.STRAWAYLINEUPGOALKEEPER to matchEvent.strAwayLineupGoalkeeper,
                        FavoriteMatch.STRAWAYLINEUPDEFENSE to matchEvent.strAwayLineupDefense,
                        FavoriteMatch.STRAWAYLINEUPMIDFIELD to matchEvent.strAwayLineupMidfield,
                        FavoriteMatch.STRAWAYLINEUPFORWARD to matchEvent.strAwayLineupForward,
                        FavoriteMatch.STRAWAYLINEUPSUBSTITUTES to matchEvent.strAwayLineupSubstitutes,
                        FavoriteMatch.STRAWAYFORMATION to matchEvent.strAwayFormation,
                        FavoriteMatch.INTHOMESHOTS to matchEvent.intHomeShots,
                        FavoriteMatch.INTAWAYSHOTS to matchEvent.intAwayShots,
                        FavoriteMatch.DATEEVENT to matchEvent.dateEvent,
                        FavoriteMatch.STRDATE to matchEvent.strDate,
                        FavoriteMatch.STRTIME to matchEvent.strTime,
                        FavoriteMatch.STRTVSTATION to matchEvent.strTVStation,
                        FavoriteMatch.IDHOMETEAM to matchEvent.idHomeTeam,
                        FavoriteMatch.IDAWAYTEAM to matchEvent.idAwayTeam,
                        FavoriteMatch.STRRESULT to matchEvent.strResult,
                        FavoriteMatch.STRCIRCUIT to matchEvent.strCircuit,
                        FavoriteMatch.STRCOUNTRY to matchEvent.strCountry,
                        FavoriteMatch.STRCITY to matchEvent.strCity,
                        FavoriteMatch.STRPOSTER to matchEvent.strPoster,
                        FavoriteMatch.STRFANART to matchEvent.strFanart,
                        FavoriteMatch.STRTHUMB to matchEvent.strThumb,
                        FavoriteMatch.STRBANNER to matchEvent.strBanner,
                        FavoriteMatch.STRMAP to matchEvent.strMap,
                        FavoriteMatch.STRLOCKED to matchEvent.strLocked
                )
            }
            Snackbar.make(activity.findViewById(R.id.lc_detail), "Added to Favorite", Snackbar.LENGTH_LONG).show()
        } catch (e: SQLiteConstraintException) {
            Snackbar.make(activity.findViewById(R.id.lc_detail), e.localizedMessage, Snackbar.LENGTH_LONG).show()
        }
    }

    fun removeFromFavorite(activity: Activity, id: String) {
        try {
            activity.database.use {
                delete(FavoriteMatch.TABLE_FAVORITE, "(IDEVENT = {id})",
                        "id" to id)
            }
            Snackbar.make(activity.findViewById(R.id.lc_detail), "Remove from Favorite", Snackbar.LENGTH_LONG).show()
        } catch (e: SQLiteConstraintException) {
            Snackbar.make(activity.findViewById(R.id.lc_detail), e.localizedMessage, Snackbar.LENGTH_LONG).show()
        }
    }

    fun favoriteState(activity: Activity, id: String) {
        activity.database.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
                    .whereArgs("(IDEVENT = {id})",
                            "id" to id)
            val favorite = result.parseList(classParser<MatchEvent.Event>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    fun setFavorite(menuItem: Menu, activity: Activity) {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(activity, R.drawable.ic_star_black_24dp)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(activity, R.drawable.ic_star_border_black_24dp)
    }
}