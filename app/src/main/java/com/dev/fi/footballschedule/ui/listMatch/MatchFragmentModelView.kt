package com.dev.fi.footballschedule.ui.listMatch

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.dev.fi.footballschedule.base.BaseViewModel
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.data.rest.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */


class MatchFragmentModelView(private val repository: Repository) : BaseViewModel() {
    val adapter: MatchAdapter = MatchAdapter()
    var visibilityProgressBar = MutableLiveData<Boolean>()

    fun getListMatch(context: Context, id: String, schedule: String) {
        if (schedule == "prev") {
            composite {
                repository.getPastLeague(
                        linkedMapOf(
                                "id" to id
                        )
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { visibilityProgressBar.value = true }
                        .doOnComplete { visibilityProgressBar.value = false }
                        .subscribe({
                            if (it.code() == 200) {
                                adapter.updateList(it.body()?.events)
                            }
                        }, {
                            Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                        })
            }
        } else if (schedule == "next") {
            composite {
                repository.getNextLeague(
                        linkedMapOf(
                                "id" to id
                        )
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { visibilityProgressBar.value = true }
                        .doOnComplete { visibilityProgressBar.value = false }
                        .subscribe({
                            if (it.code() == 200) {
                                adapter.updateList(it.body()?.events)
                            }
                        }, {
                            Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                        })
            }
        }
    }

    fun loadFavorite(event: List<MatchEvent.Event>) {
        adapter.updateList(event)
    }
}