package com.dev.fi.footballschedule.utils

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

@BindingAdapter("app:adapter")
fun rvAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("app:visibility")
fun isVisible(view: View, isVisible: Boolean?) {
    if (isVisible != null)
        if (isVisible)
            view.visibility = View.VISIBLE
        else
            view.visibility = View.GONE
}

@BindingAdapter("app:lineText")
fun lineText(view: TextView, value: String?) {
    if ((value != "") || (value != null)) {
        var newValue = value?.replace(";", ";\n", true)
        newValue = newValue?.replace("\n ", "\n", true)
        view.text = newValue
    }
}

@BindingAdapter("app:reformatDate")
fun reformatDate(view: TextView, value: String?) {
    var pFormat = SimpleDateFormat("yyyy-MM-dd")
    var date: Date = pFormat.parse(value)
    val nformat = SimpleDateFormat("dd MMM yyyy")
    view.text = nformat.format(date)
}

@BindingAdapter("app:setImage")
fun setImage(view: ImageView, value: String?) {

    if (value != null) Picasso.get().load(value).into(view)
}