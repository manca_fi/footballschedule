package com.dev.fi.footballschedule.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

abstract class BaseFragment<B : ViewDataBinding> : Fragment() {
    protected lateinit var activity: AppCompatActivity
    protected lateinit var dataBinding: B

    protected abstract fun getLayoutResource(): Int
    protected abstract fun mainCode()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutResource(), container, false)
        mainCode()
        return dataBinding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = context as AppCompatActivity
    }

}