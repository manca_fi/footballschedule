package com.dev.fi.footballschedule.data.model

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class AllLeague {
    data class Leagues(var leagues: List<League>?)
    data class League(var idLeague: String? = "", var strLeague: String? = "", var strSport: String? = "", var strLeagueAlternate: String? = "")
}