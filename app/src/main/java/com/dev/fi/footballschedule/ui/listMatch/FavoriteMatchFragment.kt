package com.dev.fi.footballschedule.ui.listMatch

import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.base.BaseFragment
import com.dev.fi.footballschedule.data.database.FavoriteMatch
import com.dev.fi.footballschedule.data.model.MatchEvent
import com.dev.fi.footballschedule.databinding.FragmentMatchBinding
import com.dev.fi.footballschedule.utils.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class FavoriteMatchFragment : BaseFragment<FragmentMatchBinding>() {
    private val viewModel by viewModel<MatchFragmentModelView>()

    override fun getLayoutResource(): Int = R.layout.fragment_match

    override fun mainCode() {
        dataBinding.setLifecycleOwner(this)

        dataBinding.viewModel = viewModel

        loadData()


        dataBinding.rvMatch.layoutManager = LinearLayoutManager(activity)
        dataBinding.rvMatch.setHasFixedSize(true)
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        activity.database.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<MatchEvent.Event>())
            viewModel.loadFavorite(favorite)
        }
    }

}