package com.dev.fi.footballschedule.ui.listLeague

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev.fi.footballschedule.R
import com.dev.fi.footballschedule.data.model.AllLeague
import com.dev.fi.footballschedule.databinding.ItemLeagueBinding
import com.dev.fi.footballschedule.ui.listMatch.ListMatchActivity
import com.orhanobut.logger.Logger

/**
 ****************************************
created by -manca-
.::manca.fi@gmail.com ::.
 ****************************************
 */

class LeagueAdapter : RecyclerView.Adapter<LeagueAdapter.ViewHolder>() {
    private var items: List<AllLeague.League>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_league, parent, false))

    override fun getItemCount(): Int = items?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items!![position])

    fun updateList(itemList: List<AllLeague.League>?) {
        items = itemList ?: throw NullPointerException()
        Logger.d("data insert")
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemLeagueBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: AllLeague.League) = with(itemView) {
            binding.dataLeagues = data
            setOnClickListener {
                val intent = Intent(itemView.context, ListMatchActivity::class.java)
                intent.putExtra("id", data.idLeague.toString())
                context.startActivity(intent)
            }
        }
    }
}